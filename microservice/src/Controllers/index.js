const {db} = require('../../db')

const findElementByPk = (field) => {
    try {

        result = db.filter(x => x.pk == field);

        if (result.length == 0) return {statusCode: 404, message: 'Pk not found'};

    return {statusCode: 200, data:result[0]}

    } catch (error) {
        console.log({step: 'Controller Validate', error: error.toString()});
        return({ statusCode: 500, message: error.toString()})  
    }
    
}

module.exports = { findElementByPk}