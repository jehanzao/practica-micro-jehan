const Services = require('../Services')
const { queueValidate } = require('./index');

const Validate = async(job, done) => {
    try {
        const { bucketName, elements } = job.data;

        let { statusCode, data, message } = await Services.Validate({ bucketName, elements });

        done(null, { statusCode, data, message });
        

    } catch (error) {

        console.log({ step: 'Adapters queueView', error: error.toString()});
        
        done({ statusCode: 500, message: InternalError }, null);
    }

}

const run = async() => {
    try {
        
        console.log("inicializar worker");

        queueValidate.process(Validate)
        

    } catch (error) {

        console.log(error);
    }
}

module.exports = {
    run
}