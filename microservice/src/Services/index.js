const Controllers = require('../Controllers')

const Validate = async({ bucketName, elements}) => {
    try {
        let messages = []

        let {statusCode, data, message} = await Controllers.findElementByPk(elements.field);
        
        if (statusCode === 404) return {statusCode, message} 

        messages.push(`BucketName = ${bucketName}`)
        
        messages.push('Pk encontrada')

        if (data.objectName == elements.objectName) messages.push('objectNames are the same')
        else messages.push('objectNames are diferents')

        if (data.isAnimated == elements.animated) messages.push(`the property isAnimated of your petition is the same`)
        else messages.push(`the property isAnimated of your petition is ${elements.animated} and we got ${data.isAnimated}`)
        
        return {statusCode: 200, data, message: messages};
    
    } catch (error) {
        console.log({ step: 'Services Validate', error: error.toString() });
        
        return { statusCode: 500, message: error.toString() };
    }
}

module.exports = {
    Validate
}