const db = [
    {
    pk: '0',
    objectName: 'Pelota',
    isAnimated: true,
    },
    {
    pk: '1',
    objectName: 'Limon',
    isAnimated: false,
    },
    {
    pk: '2',
    objectName: 'Bloque',
    isAnimated: true,
    },
]

module.exports = {db};