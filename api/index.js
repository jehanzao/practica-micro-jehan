const { queueValidate} = require('../microservice/src/Adapters/index')

const Validate = async ({bucketName, elements}) => {
    try {
        // console.log(bucketName, elements);

        const job = await queueValidate.add({bucketName, elements})

        const {statusCode, data, message} = await job.finished()

        console.log({statusCode, data, message});

    } catch (error) {
        
        console.log(error);
    }
    
}

const main = async() => {

    await Validate({bucketName: "Jehan ", elements: { field: '1', objectName: 'Pelota', animated: true } });
    await Validate({bucketName: "Carlos", elements: { field: '2', objectName: 'Pelota', animated: true } });
    await Validate({bucketName: "Raul", elements: { field: '3', objectName: 'Pelota', animated: true } });
    await Validate({bucketName: "Pancho", elements: { field: '4', objectName: 'Pelota', animated: true } });

}

main();